import sys

from humodem import Modem
from at_commands import Command
baudrate = 9600

if __name__ == '__main__':
    try:
        index = sys.argv[1]
    except IndexError:
        print("Index number not given")
        exit()

    print(f"index: {index}")

    try:
        device = "/dev/ttyUSB0"
        mod = Modem(device, device)
    except Exception as e:
        device = "/dev/ttyUSB1"
        mod = Modem(device, device)

    print(mod.sms_read(index))
