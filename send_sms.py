import sys

from humodem import Modem
from at_commands import Command
baudrate = 9600

if __name__ == '__main__':

    try:
        phone = sys.argv[1]
    except IndexError:
        print("Phone number not given")
        exit()

    print(f"phone: {phone}")

    try:
        device = "/dev/ttyUSB0"
        mod = Modem(device, device)
    except Exception as e:
        device = "/dev/ttyUSB1"
        mod = Modem(device, device)

    print(f"Model {mod.show_model()}")

    nmi_status = mod.enable_nmi()
    print(f"NMI status: {nmi_status}")

    cmd = Command(mod, "+CNMI", True)

    mod.ctrl_lock.acquire()
    try:
        ret = cmd.set("1")
    finally:
        mod.ctrl_lock.release()

    cmd = Command(mod, "+CNMI", True)

    mod.ctrl_lock.acquire()
    try:
        ret = cmd.set("1,1,2,2,0")
    finally:
        mod.ctrl_lock.release()

    cmd = Command(mod, "+CSMP", True)
    mod.ctrl_lock.acquire()
    try:
        ret = cmd.set("49,167,0,0")
    finally:
        mod.ctrl_lock.release()

    mod.enable_textmode(True)
    if not phone.startswith("+48"):
        phone = f"+48{phone}"

    import datetime
    response = mod.sms_send(f'{phone}', f'Test. Send date: {datetime.datetime.now()}')

    print(f"Response: ", response)
