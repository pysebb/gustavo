from humodem import Modem
baudrate = 9600


if __name__ == '__main__':
    try:
        device = "/dev/ttyUSB0"
        mod = Modem(device, device)
    except Exception as e:
        device = "/dev/ttyUSB1"
        mod = Modem(device, device)

    print(f"Model {mod.show_model()}")

    for item in mod.sms_list():
        print(item)

    for index, item in enumerate(mod.sms_list()):
        print(index, mod.sms_read(index))
